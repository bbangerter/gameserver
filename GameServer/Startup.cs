using Funq;
using Game.ServiceInterface;
using GameServer.Business;
using GameServer.Hubs;
using GameServer.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using ServiceStack;
using ServiceStack.Api.OpenApi;
using ServiceStack.Text;
using SignalRChat.Hubs;

namespace GameServer
{
    public class Startup
    {
        IConfiguration Configuration { get; }
        public Startup(IConfiguration configuration) => Configuration = configuration;

        readonly string MyAllowSpecificOrigins = "_myAllowSpecificOrigins";

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var origins = new string[] { "http://localhost:4200", "http://dgboardgames.azurewebsites.net",
                "https://dgboardgames.azurewebsites.net" };
            services.AddCors(options =>
            {
                options.AddPolicy(MyAllowSpecificOrigins, builder => builder.WithOrigins(origins)
                    .AllowAnyHeader()
                    //.AllowAnyMethod()
                    .AllowCredentials()
                    //.SetIsOriginAllowed((host) => true)
                    );
            });

            services.AddSingleton<ILobbyManager, LobbyManager>();
            services.AddSingleton<IGameManager, GameManager>();

            services.AddSignalR().AddNewtonsoftJsonProtocol();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseServiceStack(new AppHost
            {
                AppSettings = new NetCoreAppSettings(Configuration)
            });
            app.UseRouting();

            app.UseCors(MyAllowSpecificOrigins);

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHub<ChatHub>("/chat");
                endpoints.MapHub<LobbyHub>("/lobby");
                endpoints.MapHub<GameHub>("/game");
            });
        }
    }

    public class AppHost : AppHostBase
    {
        public AppHost() : base("GameServer", typeof(GameService).Assembly) { }

        public override void Configure(Container container)
        {
            SetConfig(new HostConfig()
            {
                DefaultRedirectPath = "/swagger-ui/",
                DebugMode = AppSettings.Get(nameof(HostConfig.DebugMode), false)
            });
            JsConfig.Init(new Config()
            {
                DateHandler = DateHandler.ISO8601,
                AlwaysUseUtc = true,
                TextCase = TextCase.CamelCase,
                TimeSpanHandler = TimeSpanHandler.StandardFormat
            });
            Plugins.Add(new OpenApiFeature());
        }
    }
}
