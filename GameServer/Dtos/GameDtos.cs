﻿using System.Collections.Generic;

namespace GameServer.Dtos
{
    public enum GameMessageType
    {
        UpdatePlayerList = 0
    }

    public class GameMessage
    {
        public GameMessageType Type { get; set; }
        public string Message { get; set; }
        public string PlayerId { get; set; }
    }

    public class UpdatePlayerList
    {
        public List<Player> Players { get; set; }
    }

    public class Player
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Group { get; set; }
    }
}
