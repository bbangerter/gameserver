﻿using System;
using System.Collections.Generic;

namespace GameServer.Dtos
{
    public enum MessageType
    {
        CreateGame = 0,
        JoinGame,
        LeaveGame,
        DestroyGame,
        GameList,
        StartGame,
        EndGame,
        AvailableGames
    };

    public class LobbyDto
    {
        public MessageType Type { get; set; }
        public string Message { get; set; }
    }

    public class CreateGameDto
    {
        public string Game { get; set; }
    }

    public class GameIdMessage
    {
        public Guid GameId { get; set; }
    }

    public class GameDto
    {
        public string CreatorName { get; set; }
        public string CreatorId { get; set; }
        public string GameName { get; set; }
        public Guid GameId { get; set; }
        public List<GamePlayerDto> Players { get; set; }
    }

    public class GamePlayerDto
    {
        public string Name { get; set; }
        public string Id { get; set; }
    }

    public class GameListDto
    {
        public List<GameDto> Games { get; set; }
    }

    public class AvailableGamesDto
    {
        public List<Game> Games { get; set; }
    }

    public class Game
    {
        public string Name { get; set; }
        public int MinPlayers { get; set; }
        public int MaxPlayers { get; set; }
        public string Component { get; set; }
    }

    public class GameStartDto
    {
        public Guid GameId { get; set; }
        public string Name { get; set; }
        public string CreatorId { get; set; }
        public List<string> PlayerIds { get; set; }
    }
}
