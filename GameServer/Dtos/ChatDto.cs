﻿namespace GameServer.Dtos
{
    public class ChatDto
    {
        public string PlayerName { get; set; }
        public string Message { get; set; }
        public bool ServerMessage { get; set; }
    }
}
