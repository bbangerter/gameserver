﻿using GameServer.Dtos;
using GameServer.Hubs;
using Microsoft.AspNetCore.SignalR;
using ServiceStack.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameServer.Business
{
    public class LobbyManager : ILobbyManager
    {
        private AvailableGamesDto _availableGames = null;
        private List<GameDto> _gameList = new List<GameDto>();
        private Dictionary<string, string> _connectionIds = new Dictionary<string, string>();
        private readonly IHubContext<LobbyHub> _hub;

        public LobbyManager(IHubContext<LobbyHub> hub)
        {
            _hub = hub;
        }

        public GameListDto GetGameList()
        {
            return new GameListDto()
            {
                Games = _gameList
            };
        }

        public void ProcessMessage(LobbyDto message, string playerName, string playerId)
        {
            switch (message.Type)
            {
                case MessageType.CreateGame:
                    CreateGame(message.Message, playerName, playerId);
                    break;
                case MessageType.JoinGame:
                    JoinGame(message.Message, playerName, playerId);
                    break;
                case MessageType.LeaveGame:
                    LeaveGame(message.Message, playerId);
                    break;
                case MessageType.DestroyGame:
                    DestroyGame(message.Message);
                    break;
                case MessageType.StartGame:
                    StartGame(message.Message, playerId);
                    break;
            }
        }

        private void StartGame(string message, string playerId)
        {
            var startGame = JsonSerializer.DeserializeFromString<GameIdMessage>(message);
            var game = FindGame(startGame.GameId);
            if (game == null || game.CreatorId != playerId)
            {
                return;
            }

            var players = game.Players.Select(p => p.Id).ToList();
            var gameStart = new GameStartDto()
            {
                GameId = game.GameId,
                Name = game.GameName,
                CreatorId = game.CreatorId,
                PlayerIds = players
            };

            var startMessage = new LobbyDto()
            {
                Type = MessageType.StartGame,
                Message = JsonSerializer.SerializeToString(gameStart)
            };

            foreach (var player in game.Players)
            {
                var connectionId = _connectionIds.Get(player.Id);
                _ = SendMessageToPlayer(connectionId, startMessage);
            }
        }

        public async Task SendMessageToPlayer<T>(string connectionId, T message)
        {
            await _hub.Clients.Client(connectionId).SendAsync("ClientMessage", message);
        }

        public void RemovePlayer(string playerId)
        {
            lock (_gameList)
            {
                var game = FindGameWithPlayer(playerId);
                if (game != null)
                {
                    if (game.CreatorId.Equals(playerId))
                    {
                        _gameList.Remove(game);
                    }
                    else
                    {
                        game.Players.RemoveAll(p => p.Id == playerId);
                    }
                }
            }
            lock(_connectionIds)
            {
                _connectionIds.Remove(playerId);
            }
        }

        public AvailableGamesDto GetAvailableGames()
        {
            if (_availableGames == null)
            {
                string text = System.IO.File.ReadAllText("games.json");
                _availableGames = JsonSerializer.DeserializeFromString<AvailableGamesDto>(text);
            }

            return _availableGames;
        }

        private void DestroyGame(string message)
        {
            var destroyGame = JsonSerializer.DeserializeFromString<GameIdMessage>(message);

            lock (_gameList)
            {
                var game = FindGame(destroyGame.GameId);
                if (game != null)
                {
                    _gameList.Remove(game);
                    return;
                }
            }
        }

        private void LeaveGame(string message, string playerId)
        {
            var leaveGame = JsonSerializer.DeserializeFromString<GameIdMessage>(message);

            lock (_gameList)
            {
                var game = FindGame(leaveGame.GameId);
                if (game != null)
                {
                    game.Players.RemoveAll(p => p.Id == playerId);
                    return;
                }
            }
        }

        private void JoinGame(string message, string playerName, string playerId)
        {
            var joinGame = JsonSerializer.DeserializeFromString<GameIdMessage>(message);

            lock(_gameList)
            {
                GameDto existingGame = FindGameWithPlayer(playerId);
                if (existingGame != null)
                {
                    // game aleady exists for this player
                    return;
                }

                var game = FindGame(joinGame.GameId);
                if (game == null)
                {
                    // game with given id not found
                    return;
                }

                var availableGames = GetAvailableGames();
                var gameData = availableGames.Games.Where(g => g.Name.Equals(game.GameName)).FirstOrDefault();
                if (gameData != null && gameData.MaxPlayers > game.Players.Count)
                {
                    game.Players.Add(new GamePlayerDto()
                    {
                        Name = playerName,
                        Id = playerId
                    });
                }
            }
        }

        private GameDto FindGameWithPlayer(string playerId)
        {
            return _gameList.Where(g => g.Players.Where(p => p.Id == playerId).Count() > 0).FirstOrDefault();
        }

        private GameDto FindGame(Guid gameId)
        {
            return _gameList.Where(g => g.GameId == gameId).FirstOrDefault();
        }

        private void CreateGame(string message, string playerName, string playerId)
        {
            var createGame = JsonSerializer.DeserializeFromString<CreateGameDto>(message);

            lock (_gameList)
            {
                var existingGame = FindGameWithPlayer(playerId);
                if (existingGame != null)
                {
                    // game aleady exists for this player
                    return;
                }

                _gameList.Add(new GameDto()
                {
                    CreatorName = playerName,
                    CreatorId = playerId,
                    GameName = createGame.Game,
                    GameId = Guid.NewGuid(),
                    Players = new List<GamePlayerDto>() { new GamePlayerDto() {
                        Name = playerName,
                        Id = playerId
                    } }
                });
            }
        }

        public void AddConnectionId(string playerId, string connectionId)
        {
            lock (_connectionIds)
            {
                _connectionIds.Add(playerId, connectionId);
            }
        }
    }
}
