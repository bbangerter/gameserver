﻿using GameServer.Dtos;
using GameServer.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace GameServer.Business
{
    public class GameManager : IGameManager
    {
        private List<Player> _players = new List<Player>();

        public void AddPlayer(string playerId, string playerName, string groupName)
        {
            _players.Add(new Player()
            {
                Id = playerId,
                Name = playerName,
                Group = groupName
            });
        }

        public void RemovePlayer(string playerId)
        {
            _players.RemoveAll(p => p.Id == playerId);
        }

        public List<Player> GetPlayers(string groupName)
        {
            return _players.Where(p => p.Group.Equals(groupName)).ToList();
        }
    }
}
