﻿using GameServer.Dtos;
using GameServer.Hubs;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Threading.Tasks;

namespace SignalRChat.Hubs
{
    public class ChatHub : BaseHub
    {
        public async Task ServerMessage(string message)
        {
            await SendMessageToGroup(message, false);
        }
       
        public override Task OnConnectedAsync()
        {
            var task = base.OnConnectedAsync();
            _ = SendMessageToGroup("has joined the chat", true);
            return task;
        }

        public override Task OnDisconnectedAsync(Exception exception)
        {
            _ = SendMessageToGroup("has left the chat", true);
            return base.OnDisconnectedAsync(exception);
        }

        private async Task SendMessageToGroup(string message, bool serverMessage)
        {
            await SendMessageToGroup(new ChatDto()
            {
                PlayerName = GetItem("PlayerName"),
                Message = message,
                ServerMessage = serverMessage
            });
        }
    }
}
