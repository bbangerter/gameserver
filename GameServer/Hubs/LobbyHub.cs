﻿using GameServer.Dtos;
using ServiceStack.Messaging;
using ServiceStack.Text;
using ServiceStack.Validation;
using System;
using System.Threading.Tasks;

namespace GameServer.Hubs
{
    public class LobbyHub : BaseHub
    {
        private ILobbyManager _lobbyManager;

        public LobbyHub(ILobbyManager lobbyManager)
        {
            _lobbyManager = lobbyManager;
        }

        public async Task ServerMessage(LobbyDto message)
        {
            _lobbyManager.ProcessMessage(message, GetItem("PlayerName"), GetItem("PlayerId"));
            await UpdateGameList();
        }

        private async Task SendAvailableGames()
        {
            var availableGames = _lobbyManager.GetAvailableGames();
            await SendMessageToCaller(new LobbyDto() {
                Type = MessageType.AvailableGames,
                Message = JsonSerializer.SerializeToString(availableGames)
            });
        }

        private async Task UpdateGameList()
        {
            var games = _lobbyManager.GetGameList();
            await SendMessageToGroup(new LobbyDto()
            {
                Type = MessageType.GameList,
                Message = JsonSerializer.SerializeToString(games)
            });
        }

        public override Task OnConnectedAsync()
        {
            var task = base.OnConnectedAsync();
            _lobbyManager.AddConnectionId(GetItem("PlayerId"), Context.ConnectionId);
            var games = _lobbyManager.GetGameList();
            _ = SendMessageToCaller(new LobbyDto()
            {
                Type = MessageType.GameList,
                Message = JsonSerializer.SerializeToString(games)
            });
            _ = SendAvailableGames();
            return task;
        }

        public override Task OnDisconnectedAsync(Exception exception)
        {
            _lobbyManager.RemovePlayer(GetItem("PlayerId"));
            _ = UpdateGameList();
            return base.OnDisconnectedAsync(exception);
        }
    }
}
