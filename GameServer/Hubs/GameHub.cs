﻿using GameServer.Dtos;
using GameServer.Interfaces;
using ServiceStack.Text;
using System;
using System.Threading.Tasks;

namespace GameServer.Hubs
{
    public class GameHub : BaseHub
    {
        private IGameManager _gameManager;

        public GameHub(IGameManager gameManager)
        {
            _gameManager = gameManager;
        }

        public async Task ServerMessage(GameMessage message)
        {
            await SendMessageToGroup(message);
        }

        public override Task OnConnectedAsync()
        {
            var task = base.OnConnectedAsync();
            _gameManager.AddPlayer(GetItem("PlayerId"), GetItem("PlayerName"), GetItem("GroupName"));
            UpdatePlayerList();
            return task;
        }

        public override Task OnDisconnectedAsync(Exception exception)
        {
            _gameManager.RemovePlayer(GetItem("PlayerId"));
            UpdatePlayerList();
            return base.OnDisconnectedAsync(exception);
        }

        private void UpdatePlayerList()
        {
            _ = SendMessageToGroup(new GameMessage()
            {
                Type = GameMessageType.UpdatePlayerList,
                Message = JsonSerializer.SerializeToString(_gameManager.GetPlayers(GetItem("GroupName")))
            });
        }

    }
}
