﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Threading.Tasks;

namespace GameServer.Hubs
{
    public class BaseHub : Hub
    {
        public override Task OnConnectedAsync()
        {
            var group = Context.GetHttpContext().Request.Query["GroupName"];
            JoinGroup(group);

            var player = Context.GetHttpContext().Request.Query["PlayerName"];
            Context.Items.Add("PlayerName", player.ToString());

            var playerId = Context.GetHttpContext().Request.Query["PlayerId"];
            Context.Items.Add("PlayerId", playerId.ToString());

            return base.OnConnectedAsync();
        }

        public override Task OnDisconnectedAsync(Exception exception)
        {
            LeaveGroup(GetItem("GroupName"));
            return base.OnDisconnectedAsync(exception);
        }

        protected async Task SendMessageToGroup<T>(T message)
        {
            await Clients.Group(GetItem("GroupName")).SendAsync("ClientMessage", message);
        }

        protected async Task SendMessageToCaller<T>(T message)
        {
            await Clients.Caller.SendAsync("ClientMessage", message);
        }

        protected Task JoinGroup(string groupName)
        {
            Context.Items.Add("GroupName", groupName);
            return Groups.AddToGroupAsync(Context.ConnectionId, groupName);
        }

        protected Task LeaveGroup(string groupName)
        {
            return Groups.RemoveFromGroupAsync(Context.ConnectionId, groupName);
        }

        protected string GetItem(string key)
        {
            object item;
            if (Context.Items.TryGetValue(key, out item))
            {
                return item.ToString();
            }

            return "";
        }
    }
}
