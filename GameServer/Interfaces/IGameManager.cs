﻿using GameServer.Dtos;
using System.Collections.Generic;

namespace GameServer.Interfaces
{
    public interface IGameManager
    {
        void AddPlayer(string playerId, string playerName, string groupName);
        void RemovePlayer(string playerId);
        List<Player> GetPlayers(string groupName);
    }
}
