﻿using GameServer.Dtos;

namespace GameServer
{
    public interface ILobbyManager
    {
        void ProcessMessage(LobbyDto message, string playerName, string playerId);
        GameListDto GetGameList();
        void RemovePlayer(string playerName);
        AvailableGamesDto GetAvailableGames();
        void AddConnectionId(string playerId, string connectionId);
    }
}
